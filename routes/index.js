var express = require('express');
var passport = require('../utils/passport');
var router = express.Router();
var User = require('../db/user')
/* GET home page. */

router.get('/', passport.isLoggedIn, function(req, res, next) {

	res.render('index', { title: 'Express' });
	console.log('this is the passport:' + req.session.passport.user)
	console.log('this is the passport:' + req.user)
	console.log('this is the passport:' + req)
	console.log('this is the passport:' + req.session.user)

	console.log('this is the passport:' + req.session3)
	console.log('this is the passport:' + res)
	console.log('this is the passport:' + res.session)
});

router.get('/login', function(req, res, next) {
	res.render('login', { title: 'Express' });
});

router.post('/login', 
	passport.authenticate(
	'local-login',{
		successRedirect : '/',
		failureRedirect : '/login',
		failureFlash : true
	}
));

function isLoggedIn (req, res, next) {
	console.log("User: " + req.user)
    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/login');
};

module.exports = router;
