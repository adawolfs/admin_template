var mongoose = require('mongoose');
var db = require('./connection');
var exports = module.exports

var UserSchema = new mongoose.Schema({
    email: String,
    alias: String,
    isAdmin: Boolean,
    role: String,
    passwordHash: String,
    passwordSalt: String

});

module.exports = mongoose.model('User', UserSchema);