var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost:27017/backend');
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function (callback) {
	exports.collection = function(name,callback){
		callback(mongoose.model(name))
	}
});
module.exports = exports