var passport = require('passport')
var LocalStrategy   = require('passport-local').Strategy;

// load up the user model
var User  = require('../db/user');

// expose this function to our app using module.exports



// =========================================================================
// passport session setup ==================================================
// =========================================================================
// required for persistent login sessions
// passport needs ability to serialize and unserialize users out of session

// used to serialize the user for the session
passport.serializeUser(function(user, done) {
    console.log("serialize")
    console.log(user.email)
    done(null, user.id);
});

// used to deserialize the user
passport.deserializeUser(function(id, done) {
    console.log("deserialize")
    User.findById(id, function(err, user) {
      console.log(user.email)
        done(err, user);
    });
});

// =========================================================================
// LOCAL SIGNUP ============================================================
// =========================================================================
// we are using named strategies since we have one for login and one for signup
// by default, if there was no name, it would just be called 'local'

passport.use('local-login', new LocalStrategy(
  {
        // by default, local strategy uses username and password, we will override with email
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true // allows us to pass back the entire request to the callback
  },
  function(req, username, password, done) {    
    console.log("testfdsd")
    User.findOne({ email: username }, function (err, user) {
      console.log("testfdsd")
      console.log(err)
      console.log(username)
      if (err) { return done(err); }
      /*if (!user) {
        return done(null, false, { message: 'Incorrect username.' });
      }
      if (!user.validPassword(password)) {
        return done(null, false, { message: 'Incorrect password.' });
      }*/

      return done(null, user);
    });
  }
));

passport.isLoggedIn = function(req, res, next) {
    console.log("User: " + req.user)
    // if user is authenticated in the session, carry on 
    if (req.isAuthenticated())
        return next();

    // if they aren't redirect them to the home page
    res.redirect('/login');
};

module.exports = passport